<?php

/**
 * @file
 * Hooks specific to the OpenID Connect Discovery module.
 */

/**
 * @defgroup openid_connect_discovery: Hooks
 * @{
 */

use Drupal\Core\Url;

/**
 * Alter the discovery data.
 *
 * @param array $data
 *   The discovery data.
 */
function hook_openid_connect_discovery_alter(&$data) {
  $data['registration_endpoint'] = Url::fromRoute('openid_connect_dynamic_registration.registration')
    ->setAbsolute()
    ->toString();
}

/**
 * @} End of "defgroup openid_connect_discovery".
 */
