<?php

namespace Drupal\openid_connect_discovery\EventSubscriber;

use Drupal\Core\Url;
use Drupal\webfinger\JsonRdLink;
use Drupal\webfinger\WebfingerEvents;
use Drupal\webfinger\Event\WebfingerResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * OpenID Connect Discovery subscriber.
 */
class OpenIdConnectDiscoverySubscriber implements EventSubscriberInterface {

  /**
   * Event handler callback.
   *
   * @param \Drupal\webfinger\Event\WebfingerResponseEvent $event
   *   The webfinger response event.
   *
   * @throws \Exception
   */
  public function onWebfingerBuildResponse(WebfingerResponseEvent $event) {
    /** @var \Drupal\webfinger\JsonRd $json_rd */
    $json_rd = $event->getJsonRd();
    $request = $event->getRequest();

    $rel = $request->query->get('rel') ?: '';
    if (empty($rel)
      || $rel !== 'http://openid.net/specs/connect/1.0/issuer') {
      return;
    }

    $resource = $request->query->get('resource') ?: '';
    if (empty($resource)) {
      return;
    }

    // We currently assume the local server is responsible for all the users.
    // @see https://openid.net/specs/openid-connect-discovery-1_0.html#Examples
    $json_rd->setSubject(parse_url($resource, PHP_URL_SCHEME));
    if (parse_url($resource, PHP_URL_SCHEME) === 'acct'
        || parse_url($resource, PHP_URL_SCHEME) === 'https'
        || parse_url($resource, PHP_URL_SCHEME) === 'http') {
      $url = Url::fromUri('internal:/')->setAbsolute()->toString(TRUE)->getGeneratedUrl();
      $json_rd->setSubject($resource);
      $link = new JsonRdLink();
      $link
        ->setRel('http://openid.net/specs/connect/1.0/issuer')
        ->setHref($url);
      $json_rd->addLink($link);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[WebfingerEvents::WEBFINGER_BUILD_RESPONSE][] = ['onWebfingerBuildResponse'];
    return $events;
  }

}
